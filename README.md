# AddThis Social Share

Add This module provides Drupal integration to addthis.com link sharing service.
AddThis is known for our beautifully simple share buttons. But we also offer a
full suite of website tools like list builder, link promotion, and recommended
posts, all for free. Integration has been implemented as a field and block.

Description from addthis.com:

The AddThis button spreads your content across the Web by making it easier for
your visitors to bookmark and share it with other people to get more likes,
shares and follows with smart website tools. This simple yet powerful button
is very easy to install and provides valuable Analytics about the bookmarking
and sharing activity of your users. AddThis helps your visitors create a buzz
for your site and increase its popularity and ranking.

AddThis share buttons, targeting tools and content recommendations help you get
more likes, shares and followers and keep them coming back. AddThis is already
on hundreds of thousands of websites including SAP, TIME Magazine, Oracle,
Freewebs, Entertainment Weekly, Topix, Lonely Planet, MapQuest, MySpace,
PGA Tour, Tower Records, Squidoo, Zappos, Funny or Die, FOX, ABC, CBS, Glamour,
PostSecret, WebMD, American Idol, and ReadWriteWeb, just to name a few. Each
month our button is displayed 20 billion times.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

For link sharing statistics registration at [Addthis.com](https://addthis.com/)
is required, but the module will work even without registration.


## Included modules

1. **addthis** - Provides the base API to integrate with AddThis. Also creates
   RenderElements, base Twig templates and global admin settings for AddThis.
   Does not provide any rendering functionality on its own.
2. **addthis_share_block** - Provides a configurable block for displaying
   AddThis.
3. **addthis_fields** - Provides two field formatters for AddThis to allow for
   rendering on entities.


## Configuration

Use the admin configuration page to configure settings.


## Development

Please see the addthis.api.php for implementation options of different displays
and altering configuration on rendering.
