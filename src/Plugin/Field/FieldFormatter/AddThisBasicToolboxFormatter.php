<?php
/**
 * @file
 * Contains \Drupal\addthis_social_share\Plugin\Field\FieldFormatter\AddThisBasicToolboxFormatter.
 */

namespace Drupal\addthis_social_share\Plugin\Field\FieldFormatter;

use Drupal\addthis_social_share\AddThisBasicToolboxFormTrait;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'addthis_share_toolbox' formatter.
 *
 * @FieldFormatter(
 *   id = "addthis_share_toolbox",
 *   label = @Translation("AddThis Basic Toolbox"),
 *   field_types = {
 *     "addthis"
 *   }
 * )
 */
class AddThisBasicToolboxFormatter extends FormatterBase {

  use AddThisBasicToolboxFormTrait;
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'share_services' => 'facebook,twitter',
      'buttons_size' => 'addthis_48x48_style',
      'counter_orientation' => 'horizontal',
      'extra_css' => '',
      'display_more_button' => 1,
      'float_options' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element = $this->addThisBasicToolboxForm($settings);

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    return array(
      '#type' => 'addthis_share_toolbox',
      '#size' => $settings['buttons_size'],
      '#services' => $settings['share_services'],
      '#extra_classes' => $settings['extra_css'],
      '#display_more_button' => $settings['display_more_button'],
      '#float_options' => $settings['float_options'],
    );
  }

}