<?php
/**
 * @file
 * Contains \Drupal\addthis_social_share\Plugin\Field\FieldFormatter\AddThisFormatter.
 */

namespace Drupal\addthis_social_share\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'addthis_share_disabled' formatter.
 *
 * @FieldFormatter(
 *   id = "addthis_share_disabled",
 *   label = @Translation("AddThis Disabled"),
 *   field_types = {
 *     "addthis"
 *   }
 * )
 */
class AddThisFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    //@TODO Implement viewElements()
    $element = array();
    $display_type = 'addthis_share_disabled';


    $markup = array(
      '#display' => array(),
    );

    return $markup;
  }
}