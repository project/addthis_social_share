<?php
/**
 * @file
 * Contains \Drupal\addthis_social_share\Plugin\Block\AddThisBlock.
 */

namespace Drupal\addthis_social_share\Plugin\Block;

use Drupal\addthis_social_share\AddThisBasicButtonFormTrait;
use Drupal\addthis_social_share\AddThisBasicToolboxFormTrait;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "addthis_share_block",
 *   admin_label = @Translation("AddThis Social Share"),
 *   category = @Translation("Blocks")
 * )
 */
class AddThisBlock extends BlockBase {

  use AddThisBasicButtonFormTrait;
  use AddThisBasicToolboxFormTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'display_type' => 'addthis_share_button',
      'basic_toolbox' => $this->addThisBasicToolboxGetDefaults(),
      'basic_button' => $this->addThisBasicButtonGetDefaults(),
    );
  }

  /**
   * {@inheritdoc}
   */
  function blockForm($form, FormStateInterface $form_state) {
    // This method receives a sub form state instead of the full form state.
    // There is an ongoing discussion around this which could result in the
    // passed form state going back to a full form state. In order to prevent
    // future breakage because of a core update we'll just check which type of
    // FormStateInterface we've been passed and act accordingly.
    // @See https://www.drupal.org/node/2798261
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }

    $settings = $this->getConfiguration();

    //get type from config
    $default_type = $settings['display_type'];

    $selected_type = $form_state->getValue([
      'settings',
      'addthis_settings',
      'display_type'
    ]);

    $selected_type = isset($selected_type) ? $selected_type : $default_type;


    $form['addthis_settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Display settings',

    );

    $form['addthis_settings']['display_type'] = array(
      '#type' => 'select',
      '#title' => t('Formatter for @title', array('@title' => 'AddThis block')),
      '#title_display' => 'invisible',
      '#options' => [
        'addthis_share_button' => 'AddThis Basic Button',
        'addthis_share_toolbox' => 'AddThis Basic Toolbox',
      ],
      '#default_value' => isset($default_type) ? $default_type : 'addthis_share_button',
      '#attributes' => array('class' => array('addthis-display-type')),
      '#ajax' => array(
        'callback' => array($this, 'addthisAjaxCallback'),
        'wrapper' => 'addthis_type_settings',
      ),
    );
    $form['addthis_settings']['type_settings'] = array(
      '#prefix' => '<div id="addthis_type_settings"',
      '#suffix' => '</div>',
    );
    if (isset($selected_type) && $selected_type == 'addthis_share_toolbox') {
      $basicToolbox = $this->addThisBasicToolboxForm($settings['basic_toolbox']);
      $form['addthis_settings']['type_settings']['basic_toolbox'] = $basicToolbox;
    }
    else {
      if (isset($selected_type) && $selected_type == 'addthis_share_button') {
        $basicButton = $this->addThisBasicButtonForm($settings['basic_button']);
        $form['addthis_settings']['type_settings']['basic_button'] = $basicButton;
      }
    }


    return $form;
  }

  /**
   * Callback for AddThisBlock blockForm() to control sub-settings based on display type.
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   */
  public function addthisAjaxCallback(array $form, FormStateInterface $form_state) {
    return $form['settings']['addthis_settings']['type_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    //@TODO Settings for unselected type get wiped because they dont exist in form.
    // Try not to overwrite them if possible.
    $this->configuration['display_type'] = $form_state->getValue([
      'addthis_settings',
      'display_type'
    ]);

    //Handle saving the partial elements provided by AddThisBasicToolboxFormTrait.
    $basicToolboxKeys = $this->addThisBasicToolboxGetDefaults();
    foreach ($basicToolboxKeys as $key => $value) {
      $this->configuration['basic_toolbox'][$key] = $form_state->getValue([
        'addthis_settings',
        'type_settings',
        'basic_toolbox',
        $key
      ]);
    }

    //Handle saving the partial elements provided by AddThisBasicButtonFormTrait.
    $basicButtonKeys = $this->addThisBasicButtonGetDefaults();
    foreach ($basicButtonKeys as $key => $value) {
      $this->configuration['basic_button'][$key] = $form_state->getValue([
        'addthis_settings',
        'type_settings',
        'basic_button',
        $key
      ]);
    }


  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    switch ($config['display_type']) {
      case 'addthis_share_button':
      return [
        '#type' => 'addthis_share_button',
        '#size' => $config['basic_button']['button_size'],
        '#extra_classes' => $config['basic_button']['extra_css'],
      ];
      break;
      case 'addthis_share_toolbox':
      return [
        '#type' => 'addthis_share_toolbox',
        '#size' => $config['basic_toolbox']['buttons_size'],
        '#services' => $config['basic_toolbox']['share_services'],
        '#extra_classes' => $config['basic_toolbox']['extra_css'],
        '#display_more_button' => $config['basic_toolbox']['display_more_button'],
        '#float_options' => $config['basic_toolbox']['float_options'],
        '#counter_orientation' => $config['basic_toolbox']['counter_orientation'],
      ];
      break;
    }

    return [
      '#markup' => ''
    ];
  }
}
